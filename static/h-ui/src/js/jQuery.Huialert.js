/* =======================================================================
 * jQuery.Huialert.js alert
 * ========================================================================*/
!function($) {
	'use strict';
	$.Huialert = function() {
		$.Huihover('.Huialert i');
		$(document).on("click",".Huialert i",function() {
			var Huialert = $(this).parents(".Huialert");
			Huialert.fadeOut("normal",function() {
				Huialert.remove();
			});
		});
	}
	$.Huialert();
} (window.jQuery);